package main

import (
	"io/ioutil"
	"math/rand"
	"os"
	"regexp"
	"strconv"
	"strings"
	"testing"
)

func TestCachedRequests(t *testing.T) {
	tmpFh, err := ioutil.TempFile(os.TempDir(), "cache-file-")
	if err != nil {
		t.Fatalf("Could not create temporary file for test: %s", err)
	}
	defer os.Remove(tmpFh.Name())

	err = InitCollectorMetrics(tmpFh.Name())
	if err != nil {
		t.Fatalf("Failed to initialize the collector metrics: %s", err)
	}

	num := rand.Intn(100)
	for i := 0; i < num; i++ {
		collectorMetrics.IncCacheRequests()
	}

	collectorMetrics.printMetrics()
	content, err := ioutil.ReadAll(tmpFh)
	if err != nil {
		t.Fatalf("Failed to read the collected metrics: %s", err)
	}

	re := regexp.MustCompile(`\nbridgestrap-cached-requests ([0-9]*)\n`)
	cachedRequests := re.Find(content)
	if len(cachedRequests) == 0 {
		t.Fatalf("Didn't find cached requests in the metrics")
	}

	strNum := strings.Split(string(cachedRequests), " ")[1]
	numCachedRequests, err := strconv.Atoi(strings.TrimSpace(strNum))
	if err != nil {
		t.Fatalf("Failed to convert the cached requests (%s) to integer: %s", string(cachedRequests), err)
	}
	if numCachedRequests != num {
		t.Errorf("Reported %d cached requests, but expected %d", numCachedRequests, num)
	}
}

func TestBridgeTests(t *testing.T) {
	const fingerprint1 = "0123456789ABCDEF0123456789ABCDEF01234567"
	const fingerprint2 = "012345670123456789ABCDEF0123456789ABCDEF"
	fingerprint2Hash, _ := hashFingerprint(fingerprint2)

	tmpFh, err := ioutil.TempFile(os.TempDir(), "cache-file-")
	if err != nil {
		t.Fatalf("Could not create temporary file for test: %s", err)
	}
	defer os.Remove(tmpFh.Name())

	err = InitCollectorMetrics(tmpFh.Name())
	if err != nil {
		t.Fatalf("Failed to initialize the collector metrics: %s", err)
	}

	collectorMetrics.AddBridgeTest(true, fingerprint1, "1.2.3.4:443 "+fingerprint1)
	collectorMetrics.AddBridgeTest(false, "", "4.3.2.1:443 "+fingerprint2)
	collectorMetrics.AddBridgeTest(false, "", "8.7.6.4:443")

	collectorMetrics.printMetrics()
	content, err := ioutil.ReadAll(tmpFh)
	if err != nil {
		t.Fatalf("Failed to read the collected metrics: %s", err)
	}

	re := regexp.MustCompile(`bridgestrap-test ([a-z]*) (.+)\n`)
	fpTests := re.FindAll(content, -1)
	if len(fpTests) != 2 {
		t.Fatalf("Found %d tests with fingerprint", len(fpTests))
	}
	for _, test := range fpTests {
		sp := strings.Split(string(test), " ")

		expectedFunctional := true
		if strings.TrimSpace(string(sp[2])) == fingerprint2Hash {
			expectedFunctional = false
		}

		functional, err := strconv.ParseBool(sp[1])
		if err != nil {
			t.Errorf("Can't parse the status of the bridge to bool: %s", err)
		}
		if functional != expectedFunctional {
			t.Errorf("Not the expected functional %v for %s", functional, string(sp[2]))
		}
	}

	re = regexp.MustCompile(`bridgestrap-test ([a-z]*) *\n`)
	fpTests = re.FindAll(content, -1)
	if len(fpTests) != 1 {
		t.Fatalf("Found %d tests without fingerprint", len(fpTests))
	}

	functionalBytes := strings.Split(string(fpTests[0]), " ")[1]
	functional, err := strconv.ParseBool(functionalBytes)
	if err != nil {
		t.Errorf("Can't parse the status of the bridge to bool: %s", err)
	}
	if functional != false {
		t.Errorf("Not the expected functional %v", functional)
	}
}

func TestHashFingerprint(t *testing.T) {
	fp := "C5B7CD6946FF10C5B3E89691A7D3F2C122D2117C"
	expectedHash := "997B6FEB5A02B1F08A36CECCE9B5B3997C0A4680"

	hfp, err := hashFingerprint(fp)
	if err != nil {
		t.Fatal("Error hashing fingerprint", err)
	}

	if hfp != expectedHash {
		t.Errorf("Hashed fingerprint doesnt match %s != %s", hfp, expectedHash)
	}
}
