package main

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"
	"time"
)

const collectorResolution = 60 * 60 * 24 * time.Second //86400 seconds

type bridgeTestResult struct {
	functional      bool
	fingerprintHash string
}

// CollectorMetrics stores the metrics to produce a metrics document for CollecTor
type CollectorMetrics struct {
	logger   *log.Logger
	filename string

	cachedRequests uint
	bridgeTests    []bridgeTestResult

	//synchronization for access to bridgestrap collector
	lock sync.Mutex
}

var collectorMetrics *CollectorMetrics

// InitCollectorMetrics initializes the global variable collectorMetrics
// it must be called once before the variable is being used
func InitCollectorMetrics(metricsFilename string) error {
	metricsFile, err := os.OpenFile(metricsFilename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	collectorMetrics = new(CollectorMetrics)
	collectorMetrics.logger = log.New(metricsFile, "", 0)
	collectorMetrics.filename = metricsFilename

	go collectorMetrics.logMetrics()
	return nil
}

// IncCachedRequests increments the counter of requests that have being answered by a cached result
func (m *CollectorMetrics) IncCacheRequests() {
	m.lock.Lock()
	defer m.lock.Unlock()

	m.cachedRequests++
}

// AddBridgeTest adds the functional status of the bridge to the metrics
// fingerprint can be an empty string, and the fingerprint will be retrieved if possible from the bridgeLine
func (m *CollectorMetrics) AddBridgeTest(functional bool, fingerprint string, bridgeLine string) {
	test := bridgeTestResult{
		functional: functional,
	}

	fp := ""
	if fingerprint != "" {
		fp = fingerprint
	} else if result := Fingerprint.Find([]byte(bridgeLine)); len(result) != 0 {
		fp = string(result)
	}
	if fp != "" {
		var err error
		test.fingerprintHash, err = hashFingerprint(fp)
		if err != nil {
			log.Println("Error hashing fingerprint", fp, err)
		}
	} else if functional {
		log.Println("The bridge was functional but we couldn't get it's fingerprint:", bridgeLine)
	}

	m.lock.Lock()
	defer m.lock.Unlock()
	m.bridgeTests = append(m.bridgeTests, test)
}

// logMetrics in intervals specified by collectorResolution
func (m *CollectorMetrics) logMetrics() {
	heartbeat := time.Tick(collectorResolution)
	for range heartbeat {
		m.printMetrics()
	}
}

func (m *CollectorMetrics) printMetrics() {
	m.lock.Lock()
	defer m.lock.Unlock()

	m.logger.Println("bridgestrap-stats-end", time.Now().UTC().Format("2006-01-02 15:04:05"), fmt.Sprintf("(%d s)", int(collectorResolution.Seconds())))
	m.logger.Println("bridgestrap-cached-requests", m.cachedRequests)

	sort.Slice(m.bridgeTests, func(i, j int) bool {
		return m.bridgeTests[i].fingerprintHash < m.bridgeTests[j].fingerprintHash
	})
	for _, test := range m.bridgeTests {
		m.logger.Println("bridgestrap-test", test.functional, test.fingerprintHash)
	}

	m.cachedRequests = 0
	m.bridgeTests = nil
}

// Handler is a http handler that responds with all the past stored metrics
func (m *CollectorMetrics) Handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")

	metricsFile, err := os.OpenFile(m.filename, os.O_RDONLY, 0644)
	if err != nil {
		log.Println("Error opening", m.filename, "file for reading:", err)
		http.NotFound(w, r)
		return
	}

	if _, err := io.Copy(w, metricsFile); err != nil {
		log.Printf("copying metricsFile returned error: %v", err)
	}
}

func hashFingerprint(fp string) (string, error) {
	fpBytes, err := hex.DecodeString(fp)
	if err != nil {
		return "", err
	}

	hasher := sha1.New()
	hasher.Write(fpBytes)
	hashedFp := hex.EncodeToString(hasher.Sum(nil))
	hashedFp = strings.ToUpper(hashedFp)
	return hashedFp, nil
}
